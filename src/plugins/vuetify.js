import Vue from "vue";
import Vuetify from "vuetify/lib";

//FontAwesome
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import {
  faHouseUser,
  faLayerGroup,
  faInfoCircle,
  faAdjust,
  faBriefcase,
  faIndustry,
  faComment,
  faUserEdit,
  faPhone,
  faMobile,
  faCalendarAlt,
  faEnvelope,
  faRoad,
  faCity,
  faFlag,
  faMap,
  faPlus,
  faMinus,
  faAddressBook,
  faPaperclip,
  faTimesCircle
} from "@fortawesome/free-solid-svg-icons";

Vue.component("font-awesome-icon", FontAwesomeIcon);
library.add(
  faHouseUser,
  faLayerGroup,
  faInfoCircle,
  faAdjust,
  faBriefcase,
  faIndustry,
  faComment,
  faUserEdit,
  faPhone,
  faMobile,
  faCalendarAlt,
  faEnvelope,
  faRoad,
  faFlag,
  faMap,
  faCity,
  faPlus,
  faMinus,
  faAddressBook,
  faPaperclip,
  faTimesCircle
);
Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: "faSvg"
  }
});
